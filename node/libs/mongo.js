var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:32768/";
const dbname = 'hackdb';

module.exports.find = (collection, query, callback) => {
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db(dbname);
        dbo.collection(collection).find(query).toArray(function (err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
            callback(err, result);
        });
    });
}

module.exports.insert = (collection, data, callback) => {
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db(dbname);
        // var myobj = { name: "Company Inc", address: "Highway 37" };
        dbo.collection(collection).insertOne(data, function (err, result) {
            if (err) throw err;
            console.log("1 document inserted");
            db.close();
            callback(err, result);
        });
    });
}

module.exports.update = (collection, query, newValues, callback) => {
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db(dbname);
        console.log('update query:', query);
        console.log('newValues:', newValues);
        dbo.collection(collection).updateOne(query, newValues, function (err, result) {
            console.log("ERROR", err);
            if (err) throw err;
            console.log("1 document updated");
            // console.log(result);
            db.close();
            callback(err, result);
        });
    });
}
