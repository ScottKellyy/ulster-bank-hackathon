curl -X PATCH \
  http://localhost:3000/profile \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"address": "1, main street, somewhere" , 
	"dateOfBirth": "20 Jun 1980",
	"emailAddress": "myname@gmail.com",
	"phoneNumber": "+353088765123123",
	
	"documents:": [
			"Address Verification", "Photo ID", "Bank Statements"
		]
}'