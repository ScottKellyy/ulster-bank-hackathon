curl -X POST \
  http://localhost:3000/marketplace \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '
	[
		{"type" : "solicitor", "name": "Peter Kay and Co", "address": "1 Main street, no town"},
		{"type" : "solicitor", "name": "Jame McBee", "address": "21 Sub street, no town"},
		{"type" : "solicitor", "name": "Jack and Peter", "address": "41 No street, no town"},
		{"type" : "surveyor", "name": "Mac and sons", "address": "23 Main street, no town"},
		{"type" : "surveyor", "name": "Jane Peters", "address": "331 Main street, no town"},
		{"type" : "surveyor", "name": "Super Survery Ltd", "address": "133 Main street, no town"},
		{"type" : "surveyor", "name": "No Cost Surveys", "address": "121 Main street, no town"},
		{"type" : "valuer", "name": "Low Value Ltd", "address": "1123 Main street, no town"},
		{"type" : "valuer", "name": "No Limit Corp", "address": "5451 Main street, no town"},
		{"type" : "valuer", "name": "Martin Dempol", "address": "6561 Main street, no town"},
		{"type" : "valuer", "name": "Harry Martin", "address": "165 Main street, no town"},
		{"type" : "electrician", "name": "Electric and co", "address": "51 Main street, no town"}
	]
'