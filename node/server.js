const express = require('express');
const app = express();
const db = require('./libs/mongo');

app.use(express.json());

app.get('/', (req, res) => res.send('Hello World!'))

// app.get('/find', (req, res) => {
//     db.find('profile', { name: 'peter' }, (err, result) => {
//         console.log(result);
//         res.send(result);
//     });
// })

// app.get('/insert', (req, res) => {
//     db.insert('profile', { name: 'peter', surname: 'bland' }, (err, result) => {
//         console.log(result);
//         res.send(result);
//     });
// })

app.get('/profile', (req, res) => {
    db.find('profile', { name: 'peter' }, (err, result) => {
        console.log(result);
        res.send(result);
    });
});

app.patch('/profile', (req, res) => {
    console.log('req', req.body);
    const updates = { '$set': req.body };
    db.update('profile', { name: "peter" }, updates, (err, result) => {
        console.log(result.result);
        res.send("updated");
    });
});

app.get('/document', (req, res) => {
    db.find('documents', {}, (err, result) => {
        res.send(result);
    });
})

app.get('/marketplace/:type', (req, res) => {
    const type = req.params.type;
    console.log("marketplace type: ", type);
    db.find('marketplace', { type: type }, (err, result) => {
        console.log(result);
        res.send(result);
    });
});

app.post('/document', (req, res) => {
    const document = req.body;
    db.insert('documents', { document }, (err, result) => {
        console.log(result);
        res.send(result);
    });
})

app.post('/marketplace', (req, res) => {
    const items = req.body;
    items.forEach(element => {
        db.insert('marketplace', { "type": element.type, "name": element.name, "address": element.address }, ()=>{});
    });

    res.send("done");
})

app.get("/:collection", (req, res)=> {
    const collection = req.params.collection;
    console.log("collection :", collection);
    db.find(collection, {}, (err, result) => {
        res.send(result);
    });    
})

app.listen(3000, () => console.log('Example app listening on port 3000!'))

